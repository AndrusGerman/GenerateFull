// Package print v-19.B18.M5.D30: PrintMessage
package main

import (
	"fmt"
)

func printError(datos ...interface{}) {
	fmt.Print("ERROR:: 😭 -> ")
	for _, v := range datos {
		fmt.Print(v)
	}
	println("")
}

func printInfo(datos ...interface{}) {
	fmt.Print("INFO::: 😌 -> ")
	for _, v := range datos {
		fmt.Print(v)
	}
	println("")
}

func printIsue(datos ...interface{}) {
	fmt.Print("ISUE::: 🤔 -> ")
	for _, v := range datos {
		fmt.Print(v)
	}
	println("")
}

func printGet() {
	fmt.Print("GET:::: 🤔 -> ")
}
