// Package MCR v-19.B18.M5.D30: Creator of the controller model and routes
package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"strings"
)

func mcrGenerador(mcrData McrData) {
	// config := readConfig()
	// Crea la variables principales
	fmt.Println()
	printInfo("Generando: " + mcrData.mcrName)

	// Verifica que exite preload
	if mcrData.Preload == "" {
		mcrData.Preload = "preload"
	}

	// Crea el Modelo
	modelFile := `package models

import (
	"github.com/jinzhu/gorm"
)

// ` + mcrData.mcrName + ` Model
type ` + mcrData.mcrName + ` struct {
	gorm.Model
` + mcrData.Values + `
}

// Modos personalizados
	
// FindMany get many ` + mcrData.mcrName + `
func (` + mcrData.mcrName + `) FindMany(where ...interface{}) *[]` + mcrData.mcrName + ` {
	var value = new([]` + mcrData.mcrName + `)
	preload := GetSchemaModel(&` + mcrData.mcrName + `{})
	// Add relation
	` + mcrData.Preload + `.Find(value, where...)
	return value
}

// FindOne get one ` + mcrData.mcrName + `
func (ctx *` + mcrData.mcrName + `) FindOne(where ...interface{}) *` + mcrData.mcrName + ` {
	preload := GetSchemaModel(&` + mcrData.mcrName + `{})
	` + mcrData.Preload + `.Find(ctx, where...)
	return ctx
}

// Create one ` + mcrData.mcrName + `
func (ctx *` + mcrData.mcrName + `) Create() *` + mcrData.mcrName + ` {
	preload := GetSchemaModel(&` + mcrData.mcrName + `{})
	ctx.DeletedAt = nil
	` + mcrData.Preload + `.Create(ctx)
	return ctx
}

// Update one ` + mcrData.mcrName + `
func (ctx *` + mcrData.mcrName + `) Update(id ...interface{}) *` + mcrData.mcrName + ` {
	if err := oneOrTwo(&ctx.ID, id); err != nil {
		return ctx
	}
	preload := GetSchemaModel(&` + mcrData.mcrName + `{})
	ctx.DeletedAt = nil
	old := new(` + mcrData.mcrName + `)
	preload.Find(old, ctx.ID)
	` + mcrData.Preload + `.Model(old).Update(ctx)
	return ctx
}

// Delete one ` + mcrData.mcrName + `
func (ctx *` + mcrData.mcrName + `) Delete(id ...interface{}) *` + mcrData.mcrName + ` {
	db := GetSchemaModel(&` + mcrData.mcrName + `{})
	if err := oneOrTwo(&ctx.ID, id); err == nil {
		db.Delete(&` + mcrData.mcrName + `{}, ctx.ID)
	}
	return ctx
}

// Advance configuration
var name` + mcrData.mcrName + `S = "" //Name in SnakeCse

// T` + mcrData.mcrName + `SF Return Name in SnakeCase
func T` + mcrData.mcrName + `SF() string {
	if name` + mcrData.mcrName + `S != "" {
		return name` + mcrData.mcrName + `S
	}
	name` + mcrData.mcrName + `S = ModelToSnake(&` + mcrData.mcrName + `{})
	return name` + mcrData.mcrName + `S
}

`
	if strings.Contains(modelFile, "time.Time") {
		modelFile = strings.Replace(modelFile, "//\"time\"", "\"time\"", 1)
	}

	//Crea el controlador
	controllerFile := `package controller

import (
	"` + packageDirectori + `/models"

	` + frameworkCode["import"] + `
)

// RouteGetAll` + mcrData.mcrName + ` get all ` + mcrData.mcrName + `
func RouteGetAll` + mcrData.mcrName + frameworkCode["func_controller"] + ` {
	` + frameworkCode["return"] + `ctx.` + frameworkCode["JSONReturn"] + ` new(models.` + mcrData.mcrName + `).FindMany())
}

// RouteGetByID` + mcrData.mcrName + ` get by ID one ` + mcrData.mcrName + `
func RouteGetByID` + mcrData.mcrName + frameworkCode["func_controller"] + ` {
	id := ctx.` + frameworkCode["GetParam"] + `("id")
	` + frameworkCode["return"] + `ctx.` + frameworkCode["JSONReturn"] + `new(models.` + mcrData.mcrName + `).FindOne(id))
}


// RouteCreate` + mcrData.mcrName + ` one ` + mcrData.mcrName + `
func RouteCreate` + mcrData.mcrName + frameworkCode["func_controller"] + ` {
	var Value = new(models.` + mcrData.mcrName + `)
	ctx.` + frameworkCode["bind_json"] + `(Value) //Por Json
	` + frameworkCode["return"] + `ctx.` + frameworkCode["JSONReturn201"] + `Value.Create())
}

// RouteDeleteByID` + mcrData.mcrName + ` delete one ` + mcrData.mcrName + ` by ID
func RouteDeleteByID` + mcrData.mcrName + frameworkCode["func_controller"] + ` {
	id := ctx.` + frameworkCode["GetParam"] + `("id")
	` + frameworkCode["return"] + `ctx.` + frameworkCode["JSONReturn204"] + `new(models.` + mcrData.mcrName + `).Delete(id))
}

// RouteUpdate` + mcrData.mcrName + ` one ` + mcrData.mcrName + ` by ID
func RouteUpdate` + mcrData.mcrName + frameworkCode["func_controller"] + ` {
	var Value = new(models.` + mcrData.mcrName + `)
	id := ctx.` + frameworkCode["GetParam"] + `("id")
	Value.FindOne(id)
	ctx.` + frameworkCode["bind_json"] + `(Value) //Por Json
	` + frameworkCode["return"] + `ctx.` + frameworkCode["JSONReturn"] + `Value.Update())
}
`
	//Migración
	cfgModelFileString := readFile("models/cfg.go")
	newMigrationString := "migrate(&" + mcrData.mcrName + "{})"
	//Verifica antes de agregar
	if strings.Contains(cfgModelFileString, newMigrationString) {
		printError("ModelError: Conflicto migration '" + mcrData.mcrName + "'")
	} else {
		cfgModelFileString = strings.Replace(cfgModelFileString, "// AutoMigrate", newMigrationString+"\n\t// AutoMigrate", 1)
	}

	//Agrega la ruta
	api := readFile("route/api.go")
	if !strings.Contains(api, "} //Base") {
		printError("RouteError: '} //Base' no found")
		os.Exit(1)
	}
	apiNew := strings.Replace(api, "} //Base", `
	//`+mcrName+` Routes
	Route.`+frameworkCode["GetRoute"]+`("/`+mcrData.mcrNameMin+`", controller.RouteGetAll`+mcrData.mcrName+`)
	Route.`+frameworkCode["PostRoute"]+`("/`+mcrData.mcrNameMin+`", controller.RouteCreate`+mcrData.mcrName+`)
	Route.`+frameworkCode["PutRoute"]+`("/`+mcrData.mcrNameMin+`/`+frameworkCode["IdParam"]+`", controller.RouteUpdate`+mcrData.mcrName+`)
	Route.`+frameworkCode["GetRoute"]+`("/`+mcrData.mcrNameMin+`/`+frameworkCode["IdParam"]+`", controller.RouteGetByID`+mcrData.mcrName+`)
	Route.`+frameworkCode["DeleteRoute"]+`("/`+mcrData.mcrNameMin+`/`+frameworkCode["IdParam"]+`", controller.RouteDeleteByID`+mcrData.mcrName+`)

} //Base`, 1)
	if strings.Contains(api, `Route.`+frameworkCode["GetRoute"]+`("/`+mcrData.mcrNameMin+`", controller.RouteGetAll`+mcrData.mcrName+`)`) {
		printError("RouteError: Conflicto '" + mcrData.mcrName + "'")
		return
	}
	// Verifica los archivos
	if fileExist("controller/" + mcrData.mcrName + "Controller.go") {
		printError("ControllerError: Conflicto '" + mcrData.mcrName + "'")
		return
	}
	if fileExist("models/" + mcrData.mcrName + ".go") {
		printError("ModelError: Conflicto '" + mcrData.mcrName + "'")
		return
	}
	//Agrega MCR a sus directorios
	WriteTextFile(cfgModelFileString, "models/cfg.go")
	WriteTextFile(apiNew, "route/api.go")
	WriteTextFile(controllerFile, "controller/"+mcrData.mcrName+"Controller.go")
	WriteTextFile(modelFile, "models/"+mcrData.mcrName+".go")
	printInfo(mcrData.mcrName + " Finalizado 😎...")
}

//Lee un archivo de texto
func readFile(name string) string {
	f, err := ioutil.ReadFile(name)
	if err != nil {
		printError("Filename: "+name, err)
		os.Exit(1)
	}
	return string(f)
}

// Este archivo existe
func fileExist(directorio string) bool {
	if _, err := os.Stat(directorio); os.IsNotExist(err) {
		// does not exist
		return false
	}
	// exist
	return true
}

//WriteTextFile Crea o remplaza un archivo de texto
func WriteTextFile(text string, name string) {
	err := ioutil.WriteFile(name, []byte(text), 0644)
	if err != nil {
		printError("Filename: "+name, err)
		os.Exit(1)
	} else {
		printInfo("Generado: " + name)
	}
}
