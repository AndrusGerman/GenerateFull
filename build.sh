#!/bin/bash
echo "GenerateFull: 19.B18.M5.D30"
echo "Compilando: Windows"
GOOS=windows go build -ldflags "-s -w" -o GenerateFullWindow.exe
# GOOS=darwin go build -ldflags "-s -w" -o GenerateFullMac
echo "Compilando: Linux"
GOOS=linux go build -ldflags "-s -w" -o GenerateFullLinux
