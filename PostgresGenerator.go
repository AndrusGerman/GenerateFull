// Package PostgresGenerator v-19.B18.M5.D30: Creator struct data by database
package main

import (
	"github.com/jinzhu/inflection"

	"strings"
)

// Tabla contiene los valores de la tabla
type Tabla struct {
	TableName      string
	TableNameTitle string
	Valores        []Valores
	// Valores para el generador
	GolangStructc string
	GolangPreload string
}

// Valores contiene sus valores
type Valores struct {
	DataType string
	ColName  string
}

func getTablesAndValues() []Tabla {
	var Db = DbGetConnection()
	defer Db.Close()
	var tablas []Tabla

	var esquema = readConfig().Database.DBSchema
	// Busca todas las tablas
	Db.Raw("SELECT table_name FROM information_schema.tables WHERE table_schema = ? AND table_type = ?", esquema, "BASE TABLE").Scan(&tablas)

	// Busca los valores de esas tablas
	for ind, val := range tablas {
		tablas[ind].TableNameTitle = singularUperText(tablas[ind].TableName, "_")
		valores := []Valores{}
		Db.Raw(`SELECT attname AS col_name , atttypid::regtype  AS data_type
		FROM   pg_attribute
		WHERE  attrelid = '` + esquema + `.` + val.TableName + `'::regclass  -- table name, optionally schema-qualified
		AND    attnum > 0
		AND    NOT attisdropped
		ORDER  BY attnum`).Scan(&valores)
		for IndValor, ValorValor := range valores {
			if strings.Contains(ValorValor.ColName, "Id") {
				valores[IndValor].ColName = strings.Replace(ValorValor.ColName, "Id", "ID", 1)
			}
			valores[IndValor].DataType = parseDataType(ValorValor.DataType)
			valores[IndValor].ColName = singularUperText(valores[IndValor].ColName, "_")
			GolangStructc, preload := dataGolang(valores[IndValor].ColName, valores[IndValor].DataType)
			tablas[ind].GolangPreload += preload
			tablas[ind].GolangStructc += GolangStructc
		}
		tablas[ind].Valores = valores
	}

	return tablas
}

func removePosPreload(data string) string {
	return ""
}

func dataGolang(text string, typeData string) (string, string) {
	retornar := ""
	preload := ""

	if text == "ID" {
		return "", ""
	} else if text == "CreatedAt" {
		return "", ""
	} else if text == "UpdatedAt" {
		return "", ""
	} else if text == "DeletedAt" {
		return "", ""
	}

	// ForenKey
	if strings.Contains(text, "ID") {
		valNoID := strings.Replace(text, "ID", "", 1)
		retornar = "\t" + text
		retornar += " uint\n"
		retornar += "\t" + valNoID
		retornar += " *" + valNoID
		retornar += " `gorm:\"ForeignKey:" + text + ";AssociationForeignKey:ID\"`\n"
		preload = "preload.Preload(PreSchem( T" + valNoID + "SF))"
	} else {
		retornar = "\t" + text + " " + typeData + "\n"
	}

	return retornar, preload
}
func singularUperText(text string, spl string) string {
	if text == "id" {
		return "ID"
	}

	val := strings.Split(text, spl)
	textResul := ""
	for _, valorSpl := range val {
		if valorSpl == "id" {
			textResul += "ID"
		} else {
			singSpl := inflection.Singular(valorSpl)
			textResul += strings.Title(singSpl)
		}

	}

	return textResul
}

func parseDataType(DataType string) string {
	switch DataType {
	case "integer":
		return "int"
	case "character varying":
		return "string"
	case "timestamp without time zone":
		return "time.Time"
	case "text":
		return "string"
	default:
		return "int"
	}
}
