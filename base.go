// Package Base v-19.B18.M5.D30: Creator of the base of a project
package main

import (
	"fmt"
	"os"
	"strings"
)

func crearBase() {
	fmt.Println("")
	printInfo("Generando ficheros:")
	crearCarpetasBase()
	crearFilesBase()
	printInfo("Finalizado 😎...")
}

func crearFilesBase() {

	confiViper := `# To apply the configuration: 'GenerateFull -re'
Database:
  DBDriver: "sqlite3"
  DBSchema: "public"
# Database Configuration In Develop
DatabaseDev:
  DBName: "ExampleDev.db"
  DBPassword: ""
  DBUser: ""
  DBHost: "localhost"
  #DBParam: "sslmode=disable"
# Database Configuration In Production
DatabaseProd:
  DBName: "ExampleProd.db"
  DBPassword: ""
  DBUser: ""
  DBHost: "localhost"
  #DBParam: "sslmode=disable"
# Core Configuration
GlobalCore:
  GenerateFullVersion: "` + generateFullVersion + `"
  GopathDir: "` + packageDirectori + `"
  Framework: "` + framework + `"
`

	// Crea la configuración del controlador
	controllerCfg := `package controller

import (
	"` + packageDirectori + `/config"
	"` + packageDirectori + `/models"
 
	` + frameworkCode["import"] + `
)

// Db get connection to db
var Db = config.DbGetConnection()

// RouteInit migrate all Models
func RouteInit` + frameworkCode["func_controller"] + ` { 
	models.Migrate()
	` + frameworkCode["return"] + `ctx.` + frameworkCode["StringReturn"] + `"Migrate success")
}
`

	// Crea la configuración del Modelo
	modelsCfg := `package models

import (
	"strconv"
	"strings"
	"errors"

	"github.com/jinzhu/gorm"
    "github.com/jinzhu/inflection"
	"` + packageDirectori + `/config"
)

// Db get connection to db
var Db = config.DbGetConnection()

// Migrate all models
func Migrate() {
	// AutoMigrate
}

// PreSchem pre add schema in to model
func PreSchem(SF func() string) (string, func(db *gorm.DB) *gorm.DB) {
	a := func(db *gorm.DB) *gorm.DB {
		return db.Table(config.Schema + "." + SF())
	}
	return snakeCamelC(SF()), a
}

// StrToUint : converte string in uint
func StrToUint(val string) uint {
	n, _ := strconv.Atoi(val)
	return uint(n)
}

// oneOrTwo : si a es A-B o error
func oneOrTwo(val *uint, b []interface{}) (err error) {
	if *val == 0 && len(b) > 0 {
		dt, ok := b[0].(string)
		if ok {
			d := StrToUint(dt)
			val = &d
		}
	}
	// Not value
	if *val == 0 {
		err = errors.New("Not Number")
	}
	return err
}

// migrate one model
func migrate(val interface{}) {
	nameS := ModelToSnake(val)
	config.GetSchema(Db, nameS).AutoMigrate(val)
}

// Convert SnakeCase in CamelCase
func snakeCamelC(valor string) (total string) {
	arr := strings.SplitAfter(valor, "_")
	for _, v := range arr {
		total += strings.Title(v)
	}
	return strings.Replace(inflection.Singular(total), "_", "", 4)
}

// addForignKey : add Forign Key to element
func addForignKey(model interface{}, value string, relationModel interface{}, relationSchema ...string) {
	modelS := ModelToSnake(model)
	tabla := ModelToSnake(relationModel)
	schemaT := config.Schema
	if len(relationSchema) != 0 {
		schemaT = relationSchema[0]
	}
	config.GetSchema(Db, modelS).AddForeignKey(value, schemaT+"."+tabla+"(id)", "RESTRICT", "RESTRICT")
}

// addUniqueIndex : add unique index constrains
func addUniqueIndex(model interface{}, nameConstrains string, index ...string) {
	config.GetSchema(Db, ModelToSnake(model)).AddUniqueIndex(nameConstrains, index...)
}

// GetSchemaModel get schema one model
func GetSchemaModel(v interface{}) *gorm.DB {
	// Set las variables
	db := Db
	name := ModelToSnake(v)
	// Configura los Schema
	db = config.GetSchema(db, name)
	// Retorna los datos
	return db
}

// ModelToSnake convert model in snakeCase string
func ModelToSnake(val interface{}) string  {
	return Db.NewScope(val).GetModelStruct().TableName(Db)
}
`
	// Crea la configuración de la ruta
	RouteCfg := `package route

import (
	` + frameworkCode["cors_import"] + `
	` + frameworkCode["import"] + `
)

// Route is main var
var Route ` + frameworkCode["return_framework"] + `

// GetRutas prepare and get all routes
func GetRutas(ctx ` + frameworkCode["return_framework"] + `) ` + frameworkCode["return_framework"] + `  {
	Route = ctx
	` + frameworkCode["add_cors"] + `
	rutas()
	return  Route
}
`
	// Crea las rutas api
	apiFile := `package route

import (
	"` + packageDirectori + `/controller"
)

func rutas() {
	// Especiales
	Route.` + frameworkCode["GetRoute"] + `("/init", controller.RouteInit)
} //Base
`
	// Crea el archivo principal
	main := `package main

import (
	` + frameworkCode["import"] + `
	"` + packageDirectori + `/config"
	"` + packageDirectori + `/route"
)

func main() {
	if config.Production {
		println("\n\n\t\tEJECUTANDOSE EN MODO PROD\n\n")
	} else {
		println("\n\n\t\tEJECUTANDOSE EN MODO DEV\n\n")
	}
	// gin.SetMode(gin.ReleaseMode)
	var server = route.GetRutas(` + frameworkCode["declare_route"] + `)
	var host = ""
	` + frameworkCode["start_server"] + `
}
`

	WriteTextFile(modelsCfg, "models/cfg.go")
	WriteTextFile(controllerCfg, "controller/cfg.go")
	WriteTextFile(apiFile, "route/api.go")
	WriteTextFile(RouteCfg, "route/cfg.go")
	WriteTextFile(confiViper, "config.yml")
	baseCreateConfig()
	WriteTextFile(main, "main.go")

}

func crearCarpetasBase() {
	if err := os.Mkdir("config", 0777); err != nil {
		printError("Folder config: ", err)
	}
	if err := os.Mkdir("controller", 0777); err != nil {
		printError("Folder controller: ", err)
	}
	if err := os.Mkdir("models", 0777); err != nil {
		printError("Folder models: ", err)
	}
	if err := os.Mkdir("route", 0777); err != nil {
		printError("Folder route: ", err)
	}
}

func baseCreateConfig() {
	config := readConfig()

	// Variables
	driverDb := ""
	GormOpenDev := ""
	GormOpenProd := ""
	schemaReturn := ""
	db := config.Database
	DBDev := config.DatabaseDev
	DBProd := config.DatabaseProd
	sqlite3 := func() {
		// Add Driver
		driverDb = "github.com/jinzhu/gorm/dialects/sqlite"
		// Add Url Conection
		GormOpenDev = DBDev.DBName
		GormOpenProd = DBProd.DBName
		schemaReturn = "return db.Table(name)"
	}
	postgres := func() {
		// Add Driver
		driverDb = "github.com/jinzhu/gorm/dialects/postgres"
		// Add Url Conection
		GormOpenDev = fmt.Sprintf("host=%s port=5432 user=%s dbname=%s password=%s %s", DBDev.DBHost, DBDev.DBUser, DBDev.DBName, DBDev.DBPassword, DBDev.DBParam)
		GormOpenProd = fmt.Sprintf("host=%s port=5432 user=%s dbname=%s password=%s %s", DBProd.DBHost, DBProd.DBUser, DBProd.DBName, DBProd.DBPassword, DBProd.DBParam)
		// Add schema compatibilidad
		schemaReturn = "return db.Table(Schema + \".\" + name)"
	}
	mysql := func() {
		// Add Driver
		driverDb = "github.com/jinzhu/gorm/dialects/mysql"
		// Add Url Conection
		GormOpenDev = fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local%s", DBDev.DBUser, DBDev.DBPassword, DBDev.DBName, DBDev.DBParam)
		GormOpenProd = fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local%s", DBProd.DBUser, DBProd.DBPassword, DBProd.DBName, DBProd.DBParam)
		schemaReturn = "return db.Table(name)"
	}

	// Parse text
	dbDriver := strings.ToLower(db.DBDriver)
	db.DBDriver = dbDriver

	// Configuracion
	if strings.Contains(dbDriver, "sqlite3") {
		sqlite3()
	} else if strings.Contains(dbDriver, "postgres") {
		postgres()
	} else if strings.Contains(dbDriver, "postgres") {
		mysql()
	} else {
		printInfo("El driver  '" + db.DBDriver + "' no es reconocido.. Compatibles: 'sqlite3', 'postgres', 'mysql'")
		printInfo("Default 'postgres'")
		postgres()
	}

	// Crear el contenido de Db
	dbFile := `package config

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/jinzhu/gorm"

	// ` + dbDriver + ` driver
	_ "` + driverDb + `"
)

// Schema value
var Schema = "` + config.Database.DBSchema + `"

// DBDriver value
var DBDriver = "` + db.DBDriver + `"

// Production is enable prodution server
var Production bool

// DB Database
var DB *gorm.DB

// Runing in init
func init() {
	flag.Parse()
	//'./app prod'
	if flag.Arg(0) == "prod" {
		Production = true
	}
	// Close Database
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		<-sigchan
		DB.Close()
		os.Exit(0)
	}()
}

// DbGetConnection obtienes la coneción de la Db
func DbGetConnection() *gorm.DB {
	if DB != nil {
		return DB
	}
	db, err := gorm.Open(DBDriver, getConectionData())
	if err != nil {
		fmt.Println("failed to connect database ", err)
		os.Exit(1)
	}
	DB = db
	return DB
}

// GetSchema  table
func GetSchema(db *gorm.DB, name string) *gorm.DB {
	` + schemaReturn + `
}

// getConectionData
func getConectionData() string {
	if Production {
		return "` + GormOpenProd + `"
	}
	return "` + GormOpenDev + `"
}

`
	WriteTextFile(dbFile, "config/db.go")
}
