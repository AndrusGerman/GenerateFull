//Package DatabaseConnection v-19.B18.M5.D30: connects to the project database
package main

import (
	"os"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres" // Postgres
)

// DbGetConnection obtienes la coneción de la Db
func DbGetConnection() *gorm.DB {
	// Read config
	config := readConfig()
	// Get Conection
	db, err := gorm.Open("postgres", "host="+config.DatabaseDev.DBHost+" port=5432 user="+config.DatabaseDev.DBUser+" dbname="+config.DatabaseDev.DBName+" password="+config.DatabaseDev.DBPassword+" "+config.DatabaseDev.DBParam) //Postgres
	if err != nil {
		printError("failed to connect database ", err)
		os.Exit(1)
	}
	return db
}
