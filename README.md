# GenerateFull  v-19.B18.M5.D30
GenerateFull. Es un metaframework para generar una estrucctura MVC de manera rapida y sencilla en Go.

## Required Packages:
- Forever
    * [Gorm"](https://github.com/jinzhu/gorm)
- Database driver:
    * [Sqlite3 | "Gin, Echo, Iris"](https://github.com/mattn/go-sqlite3)
    * [Postgres | "Gin, Echo, Iris"](https://github.com/lib/pq)
- Routing
    * [gin-gionic | "Gin"](https://github.com/gin-gonic/gin)
    * [echo-framework | "Echo"](https://echo.labstack.com/)
    * [iris-kataras | "Iris"](https://github.com/kataras/iris)
- Cors Controll
    * [gin-cors | "Gin"](https://github.com/gin-contrib/cors)
    * [Echo-middleware | "Echo"](https://github.com/labstack/echo/tree/master/middleware)

## Installation Required Packages:
- Forever:
    * Gorm: `go get github.com/jinzhu/gorm`
- Database driver:
    * Sqlite3: `go get github.com/mattn/go-sqlite3`
    * Sqlite3-Gorm: `go github.com/jinzhu/gorm/dialects/sqlite`
    * Postgres: `go get github.com/lib/pq`
    * Postgres-Gorm: `go get github.com/jinzhu/gorm/dialects/postgres`
- Framework:
    * gin-gionic: `go get github.com/gin-gonic/gin`
    * gin-gionic-cors: `go get github.com/gin-contrib/cors`
	* iris: `go get github.com/kataras/iris`
    
## Create project in Gin-gionic: Comands

#### Windows
* Download [GenerateFullWindow.exe](https://gitlab.com/AndrusGerman/generatefull/raw/master/GenerateFullWindow.exe) and move it to the folder of your project. Then open a command console and execute:
* `GenerateFullWindow -c`
* Create a model controller and the routes: Example city
* `GenerateFullWindow -mcr city`
---
#### Linux
* Download [GenerateFullLinux](https://gitlab.com/AndrusGerman/generatefull/raw/master/GenerateFullLinux) and move it to the folder of your project. Then open a command console and execute:
* `chmod +x GenerateFullLinux`
* `./GenerateFullLinux -c`
* Create a model controller and the routes: Example city
* `./GenerateFullLinux -mcr city`

## Create project in Echo-framework: Comands

#### Windows
* Download [GenerateFullWindow.exe](https://gitlab.com/AndrusGerman/generatefull/raw/master/GenerateFullWindow.exe) and move it to the folder of your project. Then open a command console and execute:
* `GenerateFullWindow -f echo -c`
* Create a model controller and the routes: Example city
* `GenerateFullWindow -mcr city`
---
#### Linux
* Download [GenerateFullLinux](https://gitlab.com/AndrusGerman/generatefull/raw/master/GenerateFullLinux) and move it to the folder of your project. Then open a command console and execute:
* `chmod +x GenerateFullLinux`
* `./GenerateFullLinux -f echo -c`
* Create a model controller and the routes: Example city
* `./GenerateFullLinux -mcr city`

## Create project in Iris-kataras: Comands

#### Windows
* Download [GenerateFullWindow.exe](https://gitlab.com/AndrusGerman/generatefull/raw/master/GenerateFullWindow.exe) and move it to the folder of your project. Then open a command console and execute:
* `GenerateFullWindow -f iris -c`
* Create a model controller and the routes: Example city
* `GenerateFullWindow -mcr city`
---
#### Linux
* Download [GenerateFullLinux](https://gitlab.com/AndrusGerman/generatefull/raw/master/GenerateFullLinux) and move it to the folder of your project. Then open a command console and execute:
* `chmod +x GenerateFullLinux`
* `./GenerateFullLinux -f iris -c`
* Create a model controller and the routes: Example city
* `./GenerateFullLinux -mcr city`

## Create a model controller and routes from a database "POSTGRES"

* Create a project with the framework of your preference
* Modify the config.yml file by adding the credentials of your postgres database
* Apply the configuration by executing the command `GenerateFullWindow -re` or `./GenerateFullLinux -re`
* Generate the files with the command `GenerateFullWindow -mcrdb` or `./GenerateFullLinux -mcrdb`

## Create create foreign key relationship
 
```go
// Model User
type User struct {
	gorm.Model
    Name string
    EmailID uint
}
// Model Email
type Email struct {
	gorm.Model
    email string
}

```

* The "User" model has an EmailID and this one wants to relate to the modelor "Email"
* We will create a relationship that will automatically obtain the email data

```go
// Model User
type User struct {
	gorm.Model
    Name string
    Email *Email `gorm:"ForeignKey:EmailID;AssociationForeignKey:ID"` //Linea
    EmailID uint
}

```
* Esta linea agrego una caja para contener los datos de Email y se esta associando con
* el "ID" de Email que esta en "EmailID"

* Para que el ORM cargue ahora nuestra configuración en el modelo "User"
* en  las funciones de busquedas "FindMany" y  FindOne:
```go
// Model User
type User struct {
	gorm.Model
    Name string
    Email *Email `gorm:"ForeignKey:EmailID;AssociationForeignKey:ID"` //Linea
    EmailID uint
}

func (User) FindMany(where ...interface{}) *[]User {
	var value = new([]Branch)
	preload := preloadBranch()
	//Add relation //
	preload.Find(value, where...)
	return value
}

func (ctx *User) FindOne(where ...interface{}) *User {
	preload := preloadBranch()
	preload.Find(this, where...)
	return ctx
}
```

* Tenemos que remplazar la linea "preload" por 
*  "preload.Preload(PreSchem(TEmailSF))"
* Esto pasa el nombre del elemento que queremos escanear "TEmailSF()"
* siendo "PreSchem()" una función que agrega la configuración del esquema selecionado
```go
// Model User
type User struct {
	gorm.Model
    Name string
    Email *Email `gorm:"ForeignKey:EmailID;AssociationForeignKey:ID"` //Linea
    EmailID uint
}

// FindMany get many Branch
func (User) FindMany(where ...interface{}) *[]User {
	var value = new([]Branch)
	preload := preloadBranch()
	//Add relation //
	preload.Preload(PreSchem(TEmailSF)).Find(value, where...)
	return value
}

// FindOne get one Branch
func (ctx *User) FindOne(where ...interface{}) *User {
	preload := preloadBranch()
	preload.Preload(PreSchem(TEmailSF)).Find(ctx, where...)
	return ctx
}

```

### Información acerca de las funciones
* PreSchem  : retorna el elemento mas su schema
* TTablaSF  : retorna el nombre del  tabla esta la retorna en SnakeCase y F por funcion
* FindOne   : retorna el elemento del ID enviado o el where
* FindMany  : retorna los elementos que concidan con el where si no hay where retorna todos
* Preload   : precarga datos de la tabla que se asigne para algun tipo de relación
---
* Si en "FindOne" no agregas un Preload no se cargaran los datos de user.Email
* tambien es posible colocal .Preload anidado ejemplo "preload.Preload(dato).Preload(dato).Find(ctx,where)"
## Compile the project and migrate:

#### Windows:
Comands:
* `go build -o example.exe`
* `example.exe`
* open in the web browser if there is no migration of the created models: [this Link](http://localhost:8081/init)

now he plays with the routes he has generated


#### Linux
Comands:
* `go build -o example`
* `./example`
* open in the web browser if there is no migration of the created models: [this Link](http://localhost:8081/init)

now he plays with the routes he has generated


