// Package Main v-19.B18.M5.D30 Initiator funcion mains
package main

import (
	"os"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/inflection"
)

func createMain(fram string) {
	// Create base
	frameworkCompatibilidad(framework)
	crearBase()
}

func mcrMain() {
	config := readConfig()
	// Add code framework
	frameworkCompatibilidad(config.GlobalCore.Framework)

	// Is compatible
	if !generateFullIsCompatible(config) {
		// Is not compatible
		return
	}

	// Is Directiry Valide
	if packageDirectori != config.GlobalCore.GopathDir {
		printError("El directorio en la configuración no concide con el directorio actual")
		printIsue("Mueve el proyecto a '" + config.GlobalCore.GopathDir + "'")
		printInfo("Esto puede evitar problemas en la importaciones")
		os.Exit(1)
	}

	// Generate
	var defaulMcrData = McrData{
		Values:     "\tName string\n",
		mcrNameMin: strcase.ToSnake(inflection.Plural(mcrName)),
		mcrName:    strcase.ToCamel(inflection.Singular(mcrName)),
	}

	// Crear un MCR
	mcrGenerador(defaulMcrData)
}

func mcrDataBaseMain() {
	// Get Config by config file
	config := reconfigureMain()

	// Add code framework
	frameworkCompatibilidad(config.GlobalCore.Framework)

	// Is compatible
	generateFullIsCompatible(config)

	// Is Directiry Valide
	if packageDirectori != config.GlobalCore.GopathDir {
		printError("El directorio en la configuración no concide con el directorio actual")
		printIsue("Mueve el proyecto a '" + config.GlobalCore.GopathDir + "'")
		printInfo("Esto puede evitar problemas en la importaciones")
		os.Exit(1)
	}

	// GenerateCode
	mcrPostgresGenerator()
}

func reconfigureMain() Config {
	// Get Config by config file
	config := readConfig()

	// Add code framework
	frameworkCompatibilidad(config.GlobalCore.Framework)

	// Is compatible
	generateFullIsCompatible(config)

	// Reconfigure
	reconfiguration()
	return config
}
