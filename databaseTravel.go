// Package TravelDatabase v-19.B18.M5.D30: go through the tables
package main

import (
	"strings"
)

func mcrPostgresGenerator() {
	// Create main vars
	var base = McrData{}
	// travel of the database

	for _, values := range getTablesAndValues() {
		// Db.Raw("ALTER TABLE "+values.TableNameTitle+" ADD COLUMN MyName VARCHAR(100)")
		if mcrNotTrash(values.TableNameTitle) {
			base.mcrNameMin = values.TableName
			base.mcrName = values.TableNameTitle
			base.Values = values.GolangStructc
			base.Preload = values.GolangPreload
			mcrGenerador(base)
		}
	}
}

// mcrNotTrash detect if a value is garbage
func mcrNotTrash(name string) bool {
	switch true {
	case strings.Contains(name, "ModelHa"):
		return false
	case strings.Contains(name, "Oauth"):
		return false
	}
	return true
}
