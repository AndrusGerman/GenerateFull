// Package Reconfigure  v-19.B18.M5.D30: Read the config file and apply it to the project
package main

import "os"

// reconfiguration: Reload configuration
func reconfiguration() {
	framework = readConfig().GlobalCore.Framework
	printInfo("Recargando Nueva configuracion....")
	os.RemoveAll("config/db.go")
	baseCreateConfig()
}
