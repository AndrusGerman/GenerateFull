// Package ReadConfig v-19.B18.M5.D30: ReadConfig File
package main

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

// Config es la configuración del proyecto
type Config struct {
	Database struct {
		DBDriver string `mapstructure:"DBDriver"`
		DBSchema string `mapstructure:"DBSchema"`
	}
	DatabaseDev struct {
		DBName     string `mapstructure:"DBName"`
		DBPassword string `mapstructure:"DBPassword"`
		DBUser     string `mapstructure:"DBUser"`
		DBHost     string `mapstructure:"DBHost"`
		DBParam    string `mapstructure:"DBParam"`
	}
	DatabaseProd struct {
		DBName     string `mapstructure:"DBName"`
		DBPassword string `mapstructure:"DBPassword"`
		DBUser     string `mapstructure:"DBUser"`
		DBHost     string `mapstructure:"DBHost"`
		DBParam    string `mapstructure:"DBParam"`
	}
	GlobalCore struct {
		Framework           string `mapstructure:"Framework"`
		GopathDir           string `mapstructure:"GopathDir"`
		GenerateFullVersion string `mapstructure:"GenerateFullVersion"`
	}
}

// readConfig Read configuration
func readConfig() Config {

	// Read File configuration
	var configuration Config
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		printError("failed to connect configFile", err)
		os.Exit(1)
	}

	// ParseConfig file
	err := viper.Unmarshal(&configuration)
	if err != nil {
		fmt.Println()
		printError("failed parse configFile", err)
		os.Exit(1)
	}
	return configuration
}
