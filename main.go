// Package Main  v-19.B18.M5.D30 Initiator of functions and variables
package main

import (
	"flag"
	"os"
	"path/filepath"
	"strings"
)

// Create Main vars
var generateFullVersion = "19.B17.M3.D15"
var mcrName string
var mcrPostgres bool
var create bool
var packageDirectori string
var framework string
var reconfigure bool
var projectName string
var frameworkCode = make(map[string]string)

// McrData structc
type McrData struct {
	Values     string
	Preload    string
	mcrName    string
	mcrNameMin string
}

// Load falgs
func init() {
	flag.StringVar(&framework, "f", "gin", "'gin','echo', 'iris'")
	flag.BoolVar(&create, "c", false, "Create base of the project")
	flag.StringVar(&mcrName, "mcr", "", "Create Model,Controller and Route by input name")
	flag.BoolVar(&mcrPostgres, "mcrdb", false, "generates files by a database 'postgres'")
	flag.BoolVar(&reconfigure, "re", false, "Update Configuration")
	flag.StringVar(&projectName, "p", "app", "Project Name")

}

func main() {
	// Prepare GenerateFull
	{
		// Parse data
		flag.Parse()

		// Loading
		printInfo("GenerateFull: V-" + generateFullVersion)

		// Guardar el directorio actual
		dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
		dir = strings.Replace(dir, "\\", "/", 18)

		// Verifica que el directorio se encuentre en GOPATH

		packageDirectori = projectName
		printInfo("MainFolder: " + packageDirectori)
		// if strings.Contains(dir, "src/") == true {
		// 	ind := strings.Index(dir, "src/")
		// 	packageDirectori = dir[ind+4:]
		// 	printInfo("MainFolder: " + packageDirectori)
		// } else {
		// 	printError("No Gopath directori detect")
		// 	return
		// }
	}

	// Load GenerateFull
	{
		if mcrName != "" {

			// Create Model, Controller and Route
			mcrMain()
		} else if create == true {

			// Create base project
			createMain(framework)
		} else if mcrPostgres == true {

			// Read database
			mcrDataBaseMain()
		} else if reconfigure == true {

			// Reload configuration
			reconfigureMain()
		} else {

			// Ningun modo selecionado
			printInfo("'--help' para ver todo los parametros")
		}
	}
}
