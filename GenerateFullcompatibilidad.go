// Package GenerateFullcompat v-19.B18.M5.D30: Compatiblidad GeneretateFull
package main

import (
	"fmt"
)

func generateFullIsCompatible(a Config) bool {
	if a.GlobalCore.GenerateFullVersion != generateFullVersion {
		printError("VersionError: App: '"+a.GlobalCore.GenerateFullVersion, "'")
		dat := 0
		printInfo("If you want to continue press: 1")
		printGet()
		fmt.Scanln(&dat)
		return (dat == 1)
	}
	return true
}
