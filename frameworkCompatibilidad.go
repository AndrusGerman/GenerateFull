// Package FrameworkCompatibilidad v-19.B18.M5.D30: Load the code in memory for the selected framework

package main

import (
	"os"
)

func frameworkCompatibilidad(name string) {
	printInfo("Framework: ", name)
	switch name {
	// Gin-framework selected
	case "gin":
		frameworkCode["import"] = `"github.com/gin-gonic/gin"`
		frameworkCode["declare_route"] = `gin.Default()`
		frameworkCode["cors_import"] = `"github.com/gin-contrib/cors"`
		frameworkCode["add_cors"] = `Route.Use(cors.Default())`
		frameworkCode["start_server"] = `server.Run(host + ":8081")`
		frameworkCode["func_controller"] = `(ctx *gin.Context)`
		frameworkCode["return_framework"] = `*gin.Engine`
		frameworkCode["return"] = ``
		frameworkCode["bind_json"] = `BindJSON`
		frameworkCode["StringReturn"] = `String(200,`
		// Route call function name
		frameworkCode["GetRoute"] = "GET"
		frameworkCode["PostRoute"] = "POST"
		frameworkCode["PutRoute"] = "PUT"
		frameworkCode["DeleteRoute"] = "DELETE"
		// Echo-framework selected
		// ID url param
		frameworkCode["IdParam"] = ":id"
		// GetParam
		frameworkCode["GetParam"] = "Param"
		// Json return
		frameworkCode["JSONReturn"] = "JSON(200, "
		frameworkCode["JSONReturn204"] = "JSON(204, "
		frameworkCode["JSONReturn201"] = "JSON(201, "
	case "echo":
		frameworkCode["import"] = `"github.com/labstack/echo"`
		frameworkCode["declare_route"] = `echo.New()`
		frameworkCode["cors_import"] = `"github.com/labstack/echo/middleware"`
		frameworkCode["add_cors"] = `Route.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
	}))`
		frameworkCode["start_server"] = `server.Start(host + ":8081")`
		frameworkCode["func_controller"] = `(ctx echo.Context) error`
		frameworkCode["return_framework"] = `*echo.Echo`
		frameworkCode["return"] = `return `
		frameworkCode["bind_json"] = `Bind`
		frameworkCode["StringReturn"] = `String(200,`
		// Route call function name
		frameworkCode["GetRoute"] = "GET"
		frameworkCode["PostRoute"] = "POST"
		frameworkCode["PutRoute"] = "PUT"
		frameworkCode["DeleteRoute"] = "DELETE"
		// ID url param
		frameworkCode["IdParam"] = ":id"
		// GetParam
		frameworkCode["GetParam"] = "Param"
		// Json return
		frameworkCode["JSONReturn"] = "JSON(200, "
		frameworkCode["JSONReturn204"] = "JSON(204, "
		frameworkCode["JSONReturn201"] = "JSON(201, "

		// Iris-framework selected
	case "iris":
		frameworkCode["import"] = `"github.com/kataras/iris"`
		frameworkCode["declare_route"] = `iris.Default()`
		// Cors Code
		frameworkCode["add_cors"] = ``
		frameworkCode["start_server"] = `server.Run(iris.Addr(host + ":8081"))`
		// Controller default param
		frameworkCode["func_controller"] = `(ctx iris.Context)`
		// Framework main server name
		frameworkCode["return_framework"] = `*iris.Application`
		// Need return
		frameworkCode["return"] = ``
		// Read Json
		frameworkCode["bind_json"] = `ReadJSON`
		frameworkCode["StringReturn"] = `Writef(`
		// Route call function name
		frameworkCode["GetRoute"] = "Get"
		frameworkCode["PostRoute"] = "Post"
		frameworkCode["PutRoute"] = "Put"
		frameworkCode["DeleteRoute"] = "Delete"
		// ID url param
		frameworkCode["IdParam"] = "{id}"
		// GetParam
		frameworkCode["GetParam"] = "Params().Get"
		// Json return
		frameworkCode["JSONReturn"] = "JSON( "
		frameworkCode["JSONReturn204"] = "JSON( "
		frameworkCode["JSONReturn201"] = "JSON( "

	default:
		// Termina el programa de manera limpia retornado un error
		printError("El framework ", name, " No es soportado")
		os.Exit(1)
	}
}
